(function () {
    "use strict";
    angular.module('app', [])
        .directive('myEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 32) {
                        scope.$apply(function () {
                            element[0].value = element[0].value + ' ';
                            scope.$eval(attrs.myEnter);
                            console.log(element[0].value)
                        });

                        event.preventDefault();
                    }
                });
            };
        })
        .directive('myCtrl', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 17) {
                        scope.$apply(function () {
                            scope.$eval(attrs.myCtrl);
                        });

                        event.preventDefault();
                    }
                });
            };
        })
        .controller('Main', function ($scope) {
            let vm = this;

            const DEFAULT_TEXT = 'Apple is green. Apple is green. ';
            vm.splittedDefaultText = DEFAULT_TEXT.split(' ');
            vm.splittedDefaultText2 = DEFAULT_TEXT.split(' ');
            vm.newText = '';
            vm.idsOfSelected = [0];

            vm.selectAdditionalWord = selectAdditionalWord;
            vm.newTextChange = newTextChange;
            vm.isSelected = isSelected;

            vm.spaceEnter = spaceEnter;

            function spaceEnter() {
                if (vm.idsOfSelected.length > 1) {
                    console.log(vm.idsOfSelected)
                    for (var i = 0; i < vm.idsOfSelected.length; i++) {
                        if (i < vm.idsOfSelected.length - 1) {
                            vm.splittedDefaultText2.splice(vm.idsOfSelected[i], 1)
                        }
                    }
                    vm.idsOfSelected = [vm.idsOfSelected[vm.idsOfSelected.length - 1]];
                } else {
                    vm.idsOfSelected.push(vm.idsOfSelected[vm.idsOfSelected.length - 1] + 1)

                    vm.idsOfSelected.splice(0, 1)

                }
            }

            function selectAdditionalWord() {
                vm.idsOfSelected.push(vm.idsOfSelected[vm.idsOfSelected.length - 1] + 1);
            }

            function isSelected(index, arr) {
                for (var i = 0; i < vm.idsOfSelected.length; i++) {
                    if (vm.idsOfSelected[i] === index) {
                        return true;
                    }
                }
            }

            function newTextChange() {
                vm.splittedNewText = [];
                vm.splittedNewText2 = [];
                vm.splittedNewText = vm.newText ? vm.newText.split(' ') : [];

                vm.splittedDefaultText2[vm.splittedNewText.length - 1] = vm.splittedNewText[vm.splittedNewText.length - 1]
                if (vm.idsOfSelected.length > 1) {
                    console.log(vm.idsOfSelected)
                    for (var i = 0; i < vm.idsOfSelected.length; i++) {
                        if (i < vm.idsOfSelected.length - 1) {
                            vm.splittedDefaultText2.splice(vm.idsOfSelected[i], 1)
                        }
                    }
                    vm.idsOfSelected = [vm.idsOfSelected[vm.idsOfSelected.length - 1]];
                }
            }
        })

})();